export default (schema, exclude = []) => {
  schema.post('save', function(error, doc, next) {
    //unique error
    if (error.name === 'MongoError' && error.code === 11000) {
      for (var key in error.keyValue) {
        if (error.keyValue.hasOwnProperty(key)&&!exclude.includes(key)) {
           next(new Error(`The ${key} ${error.keyValue[key]} already exists, Please try another.`))
           break
        }
      }
    } else {
      next();
    }
  });

}
