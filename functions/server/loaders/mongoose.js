import mongoose from 'mongoose'
import consola from 'consola'
import config from '../config'
let client
export default async () => {
  if (client && client.isConnected()) {
    consola.success(' ✌️ DB client already connected')
  } else try {
    client = await mongoose.connect(config.databaseURL, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    })
    consola.success('✌️ DB loaded and connected!')
  }
  catch (e) {
    consola.error(' 🔥  connection error...', e)
    throw e
  }
  const db = client.connection.db
  client.connection.on('error', () =>
    consola.error(' 🔥  connection error...')
  )
  client.connection.on('connected', () => {
    consola.success(` ✌️  Database opened`)
  })
  client.connection.on('disconnected', () => {
    consola.warn('🛑 Database is disconnected')
  })
  return db
}
