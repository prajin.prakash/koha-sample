require = require("esm")(module /*, options*/ )
const express = require('express')
const cors = require('cors')
const app = express()
app.use(cors({origin: true}))
require('./loaders').default({
  expressApp: app
})
module.exports.app = app

