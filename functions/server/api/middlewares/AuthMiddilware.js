let auth
const initAuth = async () => {
  if (auth) {
    return
  }
  try {
    const { getAuth } = await import('firebase-admin/auth')
    await initApp()
    auth = getAuth(app)
  } catch (e) {
    console.log('initAuth', e)
  }
}

const validateFirebaseIdToken = async (req, res) => {
  try {
    await initAuth()

    if (
      (!req.headers.authorization ||
        !req.headers.authorization.startsWith('Bearer ')) &&
      !(req.cookies && req.cookies.__session)
    ) {
      res.status(403).send('Unauthorized')
      return false
    }

    let idToken
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer ')
    ) {
      // Read the ID Token from the Authorization header.
      idToken = req.headers.authorization.split('Bearer ')[1]
    } else if (req.cookies) {
      // Read the ID Token from cookie.
      idToken = req.cookies.__session
    } else {
      // No cookie
      res.status(403).send('Unauthorized')
      return false
    }

    try {
      const decodedIdToken = await auth.verifyIdToken(idToken)
      return decodedIdToken
    } catch (error) {
      res.status(403).send('Unauthorized')
      return false
    }
  } catch (e) {
    res.status(500).send(e)
    return false
  }
}
export const Auth = (req, res, next) => {
  next()
  // if (validateFirebaseIdToken(req, res)) next()
}
