import consola from 'consola'
//comon service to access mongoose
import { NotFoundError } from '../../lib/Error'
class Service {
  constructor(model) {
    this.model = model
    this.getAll = this.getAll.bind(this)
    this.get = this.get.bind(this)
    this.create = this.create.bind(this)
    this.update = this.update.bind(this)
    this.delete = this.delete.bind(this)
    this.createOrUpdate = this.createOrUpdate.bind(this)
    this.find = this.find.bind(this)
    this.findAndUpdate = this.findAndUpdate.bind(this)
    this.findAll = this.findAll.bind(this)
    this.findAllAndUpdate = this.findAllAndUpdate.bind(this)
  }

  async getAll(query, select = '', tenant = null, populate = null) {
    let { number, size } = query

    number = number ? Number(number) : 0
    size = size ? Number(size) : 500

    const results = await this.model
      .find({ tenant })
      .select(select)
      .skip(number)
      .limit(size)
      .populate(populate)
    const total = await this.model.find({ tenant }).countDocuments()
    return { results, number, size, total }
  }

  async get(_id, select = '', tenant = null) {
    const data = await this.model.findOne({ _id, tenant }).select(select)
    if (!data) throw new NotFoundError(`${this.model.modelName} not found`)
    return { data }
  }

  async create(input) {
    const data = await this.model.create(input)
    return { data }
  }

  async update(_id, input, tenant = null) {
    const data = await this.model.findOneAndUpdate({ _id, tenant }, input, {
      new: true
    })
    if (!data) throw new NotFoundError(`${this.model.modelName} not found`)
    return { data }
  }

  async delete(_id, tenant = null) {
    const data = await this.model.findOneAndDelete({ _id, tenant })
    if (!data) throw new NotFoundError(`${this.model.modelName} not found`)
    consola.info(' remode item ', data)
    return data
  }

  async populateRelated(_id, path, select = '', match = null, tenant = null) {
    const data = await this.model
      .findOne({ _id, tenant })
      .populate({ path, select, match })
    if (!data) throw new NotFoundError(`${this.model.modelName} not found`)
    return { results: data[path] }
  }

  async createOrUpdate(find, input) {
    const data = await this.model.findOneAndUpdate(find, input, {
      upsert: true,
      new: true,
      setDefaultsOnInsert: true
    })
    return { data }
  }

  async find(find) {
    const data = await this.model.findOne(find)
    return data
  }

  async findAndUpdate(find, input) {
    const data = await this.model.findOneAndUpdate(find, input, {
      upsert: false,
      new: true
    })
    return data
  }

  async findAll(find, populate = null) {
    const data = await this.model.find(find).populate(populate)
    return data
  }
  async removeAll(find) {
    const data = await this.model.remove(find)
    return data
  }
  async findAllAndUpdate(find, input) {
    const data = await this.model.find(find, input)
    return data
  }

  async insertMany(input) {
    const data = await this.model.insertMany(input)
    return data
  }
}

export default Service
