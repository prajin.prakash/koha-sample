import expressLoader from './express'
// import mongooseLoader from './mongoose'
import consola from 'consola'

export default async ({ expressApp }) => {
  // mongooseLoader()
  // consola.success('✌️ DB loaded and connected!')

  expressLoader({ app: expressApp })
  consola.success('✌️ Express loaded')
  consola.success('✌️ Ready')
}
