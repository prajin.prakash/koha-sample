import mongoose from 'mongoose'
export default (schema, options = {}) => {
  schema.pre('save', function(next) {
    const doc = this
    const modal = options.modal || 'User'
    const field = options.field || 'registerNo'
    const countField = options.countField || 'registerYear'
    const padZero = options.padZero || '4'
    const countMatch = { tenant: doc.tenant }
    const countValue = doc[countField]
    countMatch[countField] = countValue
    mongoose.model(modal).countDocuments(countMatch, (err, count) => {
      if (err) next(err)
      doc[field] = `${countValue}${String(count + 1).padStart(padZero, '0')}`
      next()
    })
  })
}
