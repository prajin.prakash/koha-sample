import TenantService from '../services/TenantService'
import consola from 'consola'
export const AppendTenantId = async (req, res, next) => {
  const getOrigin = () => {
    let origin = req.get('origin') || req.headers.origin || ''
    if(origin === '') return 'localhost'
    origin = origin.indexOf('://') > -1 ? origin.split('/')[2] : origin.split('/')[0]
    origin = origin.split(':')[0] //remove port
    return origin.slice(0, 4) === 'www.'? origin.slice(4) : origin
  }
  let domain = getOrigin()
  consola.info(`original domain  ${domain}`)
  if (
    ['localhost', 'talenttester2022.web.app', 'free.talentester.com'].includes(
      domain
    ) &&
    req.query.origin &&
    req.query.origin.length > 3
  ) {
    domain = req.query.origin
    consola.info(`modified domain ${domain}`)
  }
  try {
    const tenant = await new TenantService().getByDomain(domain)
    if(tenant.expired)
    next(new Error('Service Expired for '+ tenant.name))
    if (!req.body) req.body = {}
    req.body.tenant = tenant
    consola.info(`Url ${req.url} request by ${tenant.domain} ${tenant.id}`, new Date())
    next()
  } catch (e) {
    next(e)
  }
}
export const copyTenantIdToParms = (req, res, next) => {
  req.params.id = req.user.tenant
  next()
}

export const copyTenantIdToBody = (req, res, next) => {
  req.body.tenant = req.user.tenant
  next()
}
